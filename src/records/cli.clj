(ns records.cli
 (:require
  [clojure.string    :as s]
  [clojure.tools.cli :refer [parse-opts]]))

(def records-cli-options
  [["-p" "--pipe-delim-file FILE" "Pipe delimited file names to read"
    :multi true
    :default []
    :update-fn conj]

   ["-c" "--comma-delim-file FILE" "Comma delimited file names to read"
    :multi true
    :default []
    :update-fn conj]

   ["-s" "--space-delim-file FILE" "Space delimited file names to read"
    :multi true
    :default []
    :update-fn conj]

   ["-o" "--output-version NUMBER" "Output version to display data"
    :id :output-version
    :parse-fn #(Integer/parseInt %)
    :default 1]

   ["-a" "--api PORT" "Run an API server"
    :id :api
    :parse-fn #(Integer/parseInt %)]
   
   ["-h" "--help"]])

(def program-description
  (str "records.main takes as input files with a set of records in one of\n"
       "three formats described below, and outputs (to the screen) the set\n"
       "of records sorted in one of three ways described below."))

(def input-description
  (str "A record consists of the following 5 fields: last name, first name,\n"
       "email, date of birth and favorite color. The input is 3 files, each\n"
       "containing records stored in a different format. The delimiters\n"
       "(commas, pipes and spaces) MUST not appear anywhere in the data\n"
       "values themselves. Dates should be input as 'yyyy-MM-dd'"))

(def delimiter-description
  (str " * The pipe-delimited file lists each record as follows:\n"
       "   LastName | FirstName | Email | FavoriteColor | DateOfBirth\n"
       " * The comma-delimited file looks like this:\n"
       "   LastName, FirstName, Email, FavoriteColor, DateOfBirth\n"
       " * The space-delimited file looks like this:\n"
       "   LastName FirstName Email FavoriteColor DateOfBirth"))

(def output-description 
  (str "One of 3 different views of your data:\n"
       " * Output 1 – sorted by email (descending). Then by last name ascending.\n"
       " * Output 2 – sorted by birth date, ascending.\n"
       " * Output 3 – sorted by last name, descending.\n"
       "\n"
       "Dates are output as 'M/d/yyy'"))

(defn- usage [options-summary]
  (->> [program-description
        ""
        "Input:"
        input-description
        ""
        delimiter-description
        ""
        "output:"
        output-description
        ""
        "Usage: clj -m records.main [options]"
        ""
        "Options:"
        options-summary
        ""
        ""
        "Please refer to the manual page for more information."]
       (s/join \newline)))

(defn- error-msg [errors]
  (str "The following errors occurred while parsing your command:\n\n"
       (s/join \newline errors)))

(defn- handle-args [args]
  (let [{:keys [options errors summary] :as opts} (parse-opts args records-cli-options)]
    (cond
      (:help options)
      {:exit-message (usage summary) :ok? true}
      errors
      {:exit-message (error-msg errors)}
      :else
      opts)))

(defn- exit [status msg]
  (println msg)
  (System/exit status))

(defn handle-cli
  "Parses CLI args and returns appropriate exit data to the CLI user if
   appropriate. If runnable options are provided, returns the options
   to the caller for program launch."
  [args]
  (let [{:keys [options exit-message ok?]} (handle-args args)]
    (if exit-message
      (exit (if ok? 0 1) exit-message)
      options)))
