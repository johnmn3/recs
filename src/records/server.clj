(ns records.server
  (:require
   [muuntaja.core                     :as m]
   [records.data                      :as data]
   [records.sort                      :as sort]
   [reitit.ring                       :as ring]
   [reitit.coercion.spec]
   [reitit.swagger                    :as swagger]
   [reitit.swagger-ui                 :as swagger-ui]
   [reitit.ring.coercion              :as coercion]
   [reitit.dev.pretty                 :as pretty]
   [reitit.ring.middleware.muuntaja   :as muuntaja]
   [reitit.ring.middleware.exception  :as exception]
   [reitit.ring.middleware.multipart  :as multipart]
   [reitit.ring.middleware.parameters :as parameters]
   [reitit.ring.middleware.dev        :as dev]
   [reitit.ring.spec                  :as spec]
   [ring.adapter.jetty                :as jetty]
   [spec-tools.spell                  :as spell]))

(def db (atom #{}))

(def delim
  {:pipe \|
   :space \space
   :comma \,})

(defn add-record!
  "Takes a map `delim-type` and `value`, where delim-type is one of `:pipe`,
   `:space` or `:comma` and where value is a single data row. Parses the data
   row and adds the resulting data map to the db."
  [{:keys [delim-type value]}]
  (let [new-row (->> value
                     (data/->>csv (delim delim-type))
                     first
                     data/mk-row)]
    (swap! db conj new-row)))

(def app
  (ring/ring-handler
   (ring/router
    [["/swagger.json"
      {:get {:no-doc true
             :swagger {:info {:title "records.server"
                              :description "submit and view records"}}
             :handler (swagger/create-swagger-handler)}}]

     ["/records"
      {:swagger {:tags ["records"]}}
      ["/"
       {:post {:summary "records with spec body parameters"
               :parameters {:body {:delim-type keyword?, :value string?}}
               :responses {200 {:body {:result vector?}}}
               :handler (fn [{{{:as ctx} :body} :parameters}]
                          (add-record! ctx)
                          {:status 200
                           :body {:result (data/format-row-date @db)}})}}]
      ["/email"
       {:get {:summary "returns records sorted by email"
              :swagger {:produces ["text/json"]}
              :parameters {:query {}}
              :responses {200 {:body {:result vector?}}}
              :handler (fn [_]
                         {:status 200
                          :body {:result (data/format-row-date (sort/by-email @db))}})}}]
      ["/birthdate"
       {:get {:summary "returns records sorted by birth date"
              :swagger {:produces ["text/json"]}
              :parameters {:query {}}
              :responses {200 {:body {:result vector?}}}
              :handler (fn [_]
                         {:status 200
                          :body {:result (data/format-row-date (sort/by-date-of-birth @db))}})}}]
      ["/name"
       {:get {:summary "returns records sorted by name (LastName then FirstName)"
              :swagger {:produces ["text/json"]}
              :parameters {:query {}}
              :responses {200 {:body {:result vector?}}}
              :handler (fn [_]
                         {:status 200
                          :body {:result (data/format-row-date (sort/by-name @db))}})}}]]]

    {:reitit.middleware/transform dev/print-request-diffs
     :validate spec/validate
     :reitit.spec/wrap spell/closed
     :exception pretty/exception
     :data {:coercion reitit.coercion.spec/coercion
            :muuntaja m/instance
            :middleware [swagger/swagger-feature
                         parameters/parameters-middleware
                         muuntaja/format-negotiate-middleware
                         muuntaja/format-response-middleware
                         exception/exception-middleware
                         muuntaja/format-request-middleware
                         coercion/coerce-response-middleware
                         coercion/coerce-request-middleware
                         multipart/multipart-middleware]}})
   (ring/routes
    (swagger-ui/create-swagger-ui-handler
     {:path "/"
      :config {:validatorUrl nil
               :operationsSorter "alpha"}})
    (ring/create-default-handler))))

(defn start
  "Starts the API server. Requires at least a port number to start on.
   Optionally takes initialization data to populate the db on startup."
  [port & [initial-data]]
  (when initial-data
    (swap! db into initial-data))
  (jetty/run-jetty #'app {:port port :join? false})
  (println "server running on port" port))

(comment
  (start 3001))
