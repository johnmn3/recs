(ns records.main
  (:require
   [clojure.pprint :as pp]
   [records.cli    :as cli]
   [records.data   :as data]
   [records.server :as server]
   [records.sort   :as sort]))

(defn run [options]
  (let [data (data/combine-data options)
        sorted-data (sort/sort-data options data)]
    (if-let [port (:api options)]
      (future (server/start port sorted-data))
      (pp/print-table (data/format-row-date sorted-data)))))

(defn -main [& args]
  (when-let [run-data (cli/handle-cli args)]
    (run run-data)))
