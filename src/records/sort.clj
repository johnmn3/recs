(ns records.sort)

(defn- multi-compare [[c & cs] [x & xs] [y & ys]]
  (if (= x y)
    (multi-compare cs xs ys)
    (c x y)))

(defn- multi-comparator [comparators]
  (fn [xs ys]
    (if (= (count xs) (count ys) (count comparators))
      (multi-compare comparators xs ys)
      (throw (RuntimeException. "count not equal")))))

(defn- sort-of
  "sort-of is sort of like sort-by, but you can also give it a flat vector of
   `map-key comparator` pairs, allowing for nested sort orders."
  [sorter data]
  (if-not (vector? sorter)
    (sort-by #(get % sorter) data)
    (let [sorts (partition 2 sorter)
          ks (mapv first sorts)
          comps (mapv second sorts)]
      (sort-by (apply juxt (map (fn [k] #(get % k)) ks))
               (multi-comparator comps)
               data))))

(defn by-email
  "Sort records by \"Email\""
  [data]
  (sort-of "Email" data))

(defn by-date-of-birth
  "Sort records by \"DateOfBirth\""
  [data]
  (sort-of "DateOfBirth" data))

(defn by-name
  "Sort records by \"LastName\" and then \"FirstName\""
  [data]
  (sort-of ["LastName" compare
            "FirstName" compare]
           data))

(defn- by-email-desc&last-name
  "Sort records by \"Email\" (descending) and then \"LastName\""
  [data]
  (sort-of ["Email"    #(compare %2 %1)
            "LastName" compare]
           data))

(defn- by-last-name-desc
  "Sort records by \"LastName\" (descending)"
  [data]
  (sort-of ["LastName" #(compare %2 %1)] data))

(defn sort-data
  "Supports the -o option of the CLI. Takes a option map with a key of
   :output-version, which can be a 1, 2 or 3, and sequence of record data.

   For output-versions:
   1 - sorts by email (descending) and then last name
   2 - sorts by birthdate
   3 - sorts by last name (descending)"
  [{:keys [output-version]} data]
  (case output-version
    1 (by-email-desc&last-name data)
    2 (by-date-of-birth data)
    3 (by-last-name-desc data)))
