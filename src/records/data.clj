(ns records.data
  (:require
   [clojure.data.csv :as csv]
   [clojure.string   :as s]
   [tick.alpha.api   :as t]
   [tick.format      :as f]))

(defn- formatter [d]
  (t/format (f/formatter "M/d/yyy") d))

(defn- update-date-of-birth [row]
  (update row "DateOfBirth" formatter))

(defn format-row-date
  "When provided a data row, updates the \"DateOfBirth\" field to a string
   format of \"M/d/yyy\" for rendering in the CLI and the API."
  [rows]
  (if-not (map? rows)
    (mapv update-date-of-birth rows)
    (update-date-of-birth rows)))

(defn ->>csv
  "Takes a separator type and either a file or string of row data. Reads the
   data out into a vector of row vectors, with row values trimmed of whitespace."
  [separator file]
  (->> (csv/read-csv file :separator separator)
       (mapv #(mapv s/trim %))))

(defn- get-data [separator filename]
  (->> filename
       slurp
       (->>csv separator)))

(defn mk-row
  "Takes a sequence of 5 elements and returns a map with each element
   associated with, in order, \"LastName\", \"FirstName\", \"Email\"
   \"FavoriteColor\" and \"DateOfBirth\". \"DateOfBirth\" must be of the
   format \"yyyy-MM-d\"."
  [row-data]
  {"LastName"      (nth row-data 0)
   "FirstName"     (nth row-data 1)
   "Email"         (nth row-data 2)
   "FavoriteColor" (nth row-data 3)
   "DateOfBirth"   (t/parse (nth row-data 4))})

(defn combine-data
  "When provided at the CLI with either a `-p`, `-c` or `-s` command, takes
   a map of keys `pipe-delim-file`, `comma-delim-file` and/or `space-delim-file`
   of either filenames or strings of data and processes the string data,
   combining the resulting data rows into a single sequence of records."
  [{:keys [pipe-delim-file comma-delim-file space-delim-file]}]
  (let [pipes-data (->> pipe-delim-file
                        (mapv (partial get-data \|))
                        (apply concat)
                        (mapv mk-row))
        comma-data (->> comma-delim-file
                        (mapv (partial get-data \,))
                        (apply concat)
                        (mapv mk-row))
        space-data (->> space-delim-file
                        (mapv (partial get-data \space))
                        (apply concat)
                        (mapv mk-row))
        data (->> [pipes-data comma-data space-data]
                  (apply concat))]
    data))
