(ns records.server-test
  (:require
   [clojure.test :refer [deftest testing is]]
   [records.server :refer [app]]
   [ring.mock.request :refer [request json-body]]))

(def mock-record-alice
  {:delim-type :pipe
   :value "Smith | Alice | alice@demo.com | blue | 1981-11-06"})

(def mock-record-bob
  {:delim-type :space
   :value "Smith Bob bob@demo.com green 1981-11-05"})

(def mock-record-dan
  {:delim-type :comma
   :value "Wordsworth, Dan, dan@demo.com, yellow, 1981-11-07"})

(def mock-result-1
  "{\"result\":[{\"LastName\":\"Smith\",\"FirstName\":\"Alice\",\"Email\":\"alice@demo.com\",\"FavoriteColor\":\"blue\",\"DateOfBirth\":\"11/6/1981\"}]}")

(def mock-result-2
  "{\"result\":[{\"LastName\":\"Smith\",\"FirstName\":\"Bob\",\"Email\":\"bob@demo.com\",\"FavoriteColor\":\"green\",\"DateOfBirth\":\"11/5/1981\"},{\"LastName\":\"Smith\",\"FirstName\":\"Alice\",\"Email\":\"alice@demo.com\",\"FavoriteColor\":\"blue\",\"DateOfBirth\":\"11/6/1981\"}]}")

(def mock-result-3
  "{\"result\":[{\"LastName\":\"Smith\",\"FirstName\":\"Alice\",\"Email\":\"alice@demo.com\",\"FavoriteColor\":\"blue\",\"DateOfBirth\":\"11/6/1981\"},{\"LastName\":\"Smith\",\"FirstName\":\"Bob\",\"Email\":\"bob@demo.com\",\"FavoriteColor\":\"green\",\"DateOfBirth\":\"11/5/1981\"}]}")

(def mock-result-4
  "{\"result\":[{\"LastName\":\"Smith\",\"FirstName\":\"Bob\",\"Email\":\"bob@demo.com\",\"FavoriteColor\":\"green\",\"DateOfBirth\":\"11/5/1981\"},{\"LastName\":\"Smith\",\"FirstName\":\"Alice\",\"Email\":\"alice@demo.com\",\"FavoriteColor\":\"blue\",\"DateOfBirth\":\"11/6/1981\"},{\"LastName\":\"Wordsworth\",\"FirstName\":\"Dan\",\"Email\":\"dan@demo.com\",\"FavoriteColor\":\"yellow\",\"DateOfBirth\":\"11/7/1981\"}]}")

(def mock-result-5
  "{\"result\":[{\"LastName\":\"Smith\",\"FirstName\":\"Bob\",\"Email\":\"bob@demo.com\",\"FavoriteColor\":\"green\",\"DateOfBirth\":\"11/5/1981\"},{\"LastName\":\"Smith\",\"FirstName\":\"Alice\",\"Email\":\"alice@demo.com\",\"FavoriteColor\":\"blue\",\"DateOfBirth\":\"11/6/1981\"},{\"LastName\":\"Wordsworth\",\"FirstName\":\"Dan\",\"Email\":\"dan@demo.com\",\"FavoriteColor\":\"yellow\",\"DateOfBirth\":\"11/7/1981\"}]}")

(deftest example-server
  (with-redefs [records.server/db (atom #{})]

    (testing "POST"
      (is (= (-> (request :post "/records/") (json-body mock-record-alice)
                 app :body slurp)
             (-> {:request-method :post :uri "/records/" :body-params mock-record-alice}
                 app :body slurp)
             mock-result-1)))

    (testing "GET"
      (is (= (-> (request :get "/records/email")
                 app :body slurp)
             (-> {:request-method :get :uri "/records/email" :query-string ""}
                 app :body slurp)
             (-> {:request-method :get :uri "/records/email" :query-params {}}
                 app :body slurp)
             mock-result-1)))

    (testing "POST"
      (is (= (-> (request :post "/records/") (json-body mock-record-bob)
                 app :body slurp)
             (-> {:request-method :post :uri "/records/" :body-params mock-record-bob}
                 app :body slurp)
             mock-result-2)))

   (testing "GET"
     (is (= (-> (request :get "/records/name")
                app :body slurp)
            (-> {:request-method :get :uri "/records/name" :query-string ""}
                app :body slurp)
            (-> {:request-method :get :uri "/records/name" :query-params {}}
                app :body slurp)
            mock-result-3)))

   (testing "POST"
     (is (= (-> (request :post "/records/") (json-body mock-record-dan)
                app :body slurp)
            (-> {:request-method :post :uri "/records/" :body-params mock-record-dan}
                app :body slurp)
            mock-result-4)))

   (testing "GET"
     (is (= (-> (request :get "/records/birthdate")
                app :body slurp)
            (-> {:request-method :get :uri "/records/birthdate" :query-string ""}
                app :body slurp)
            (-> {:request-method :get :uri "/records/birthdate" :query-params {}}
                app :body slurp)
            mock-result-5)))))
