## `records.main`

### usage

To launch from the CLI with a file of rows with pipe delimited values:
`clj -m records.main -p pipes.txt`
Output:
```
|    LastName | FirstName |           Email | FavoriteColor | DateOfBirth |
|-------------+-----------+-----------------+---------------+-------------|
|     Lasttwo |   Pipetwo |   two@email.com |        redtwo |    1/1/1982 |
|   Lastthree | Pipethree | three@email.com |      redthree |    1/1/1983 |
| Lastthree-a | Pipethree | three@email.com |      redthree |    1/1/1983 |
| Lastthree-b | Pipethree | three@email.com |      redthree |    1/1/1983 |
|     Lastone |   Pipeone |   one@email.com |        redone |    1/1/1981 |
```

To launch from the CLI with files of rows with pipe, comma and space delimited values:
`clj -m records.main -p pipes.txt -c commas.txt -s spaces.txt -o 3`
Output:
```
|    LastName |  FirstName |           Email | FavoriteColor | DateOfBirth |
|-------------+------------+-----------------+---------------+-------------|
|     Lasttwo |    Pipetwo |   two@email.com |        redtwo |    1/1/1982 |
| Lastthree-b |  Pipethree | three@email.com |      redthree |    1/1/1983 |
| Lastthree-a |  Pipethree | three@email.com |      redthree |    1/1/1983 |
|   Lastthree |  Pipethree | three@email.com |      redthree |    1/1/1983 |
|   Lastsix-b |   Commasix |   six@email.com |        redsix |    1/1/1986 |
|   Lastsix-a |   Commasix |   six@email.com |        redsix |    1/1/1986 |
|     Lastsix |   Commasix |   six@email.com |        redsix |    1/1/1986 |
|   Lastseven | Spaceseven | seven@email.com |      redseven |    1/1/1987 |
|     Lastone |    Pipeone |   one@email.com |        redone |    1/1/1981 |
|  Lastnine-b |  Spacenine |  nine@email.com |       rednine |    1/1/1989 |
|  Lastnine-a |  Spacenine |  nine@email.com |       rednine |    1/1/1989 |
|    Lastnine |  Spacenine |  nine@email.com |       rednine |    1/1/1989 |
|    Lastfour |  Commafour |  four@email.com |       redfour |    1/1/1984 |
|    Lastfive |  Commafive |  five@email.com |       redfive |    1/1/1985 |
|   Lasteight | Spaceeight | eight@email.com |      redeight |    1/1/1988 |
```

To launch an API server intitialized with files of rows with pipe, comma and space delimited values:
`clj -m records.main -p pipes.txt -c commas.txt -s spaces.txt -a 3001`
Output:
```
2021-07-18 22:55:03.080:INFO::main: Logging initialized @6073ms to org.eclipse.jetty.util.log.StdErrLog
2021-07-18 22:55:03.285:INFO:oejs.Server:clojure-agent-send-off-pool-0: jetty-9.4.12.v20180830; built: 2018-08-30T13:59:14.071Z; git: 27208684755d94a92186989f695db2d7b21ebc51; jvm 11.0.9.1+1-LTS
2021-07-18 22:55:03.344:INFO:oejs.AbstractConnector:clojure-agent-send-off-pool-0: Started ServerConnector@1319547e{HTTP/1.1,[http/1.1]}{0.0.0.0:3001}
2021-07-18 22:55:03.344:INFO:oejs.Server:clojure-agent-send-off-pool-0: Started @6337ms
server running on port 3001
```

Then, navigate in a browser to http://localhost:3001/index.html and excercise the endpoints from swagger.

To test, run: `clj -X:test`